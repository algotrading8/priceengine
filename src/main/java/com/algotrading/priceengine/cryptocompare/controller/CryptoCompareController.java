package com.algotrading.priceengine.cryptocompare.controller;

import com.algotrading.priceengine.cryptocompare.CryptoCompareFullSymbol;
import com.algotrading.priceengine.cryptocompare.CryptoComparePrice;
import com.algotrading.priceengine.cryptocompare.service.CyrptoCompareService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping({"/cryptocompare"})
public class CryptoCompareController {

    @Autowired
    CyrptoCompareService cyrptoCompareService;

    @RequestMapping(value="/priceSymbolCcy/{symbol}/{ccy}", method = RequestMethod.GET)
    public CryptoComparePrice getPriceBySymbolCurrency(@PathVariable(value="symbol") String symbol,
                                                       @PathVariable(value="ccy") String ccy) {
        return cyrptoCompareService.getPriceBySymbolCurrency(symbol, ccy);
    }

    @RequestMapping(value="/fullSymbolCcy/{symbol}/{ccy}", method = RequestMethod.GET)
    public CryptoCompareFullSymbol getFullCoinInfoByCurrency(@PathVariable(value="symbol") String symbol,
                                                             @PathVariable(value="ccy") String ccy) {
        return cyrptoCompareService.getFullCoinInfoByCurrency(symbol, ccy);
    }

    @RequestMapping(value="/OCHLSymbolCcy/{symbol}/{ccy}/{type}/{interval}", method = RequestMethod.GET)
    public List<CryptoComparePrice> getOCHLBySymbolCurrency(@PathVariable(value="symbol") String symbol,
                                                            @PathVariable(value="ccy") String ccy,
                                                            @PathVariable(value="type") String type,
                                                            @PathVariable(value="interval") String interval) {
        return cyrptoCompareService.getOCHLBySymbolCurrency(symbol, ccy, type, interval);
    }
}
