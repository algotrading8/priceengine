package com.algotrading.priceengine.cryptocompare;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "cryptocompare")
public class CryptoCompareProp {
    private String apikey;
    private String singleSymbolPrice;
    private String multipleSymbolFull;
    private String singleSymbolHistByType;

    public String getApikey() {
        return apikey;
    }

    public void setApikey(String apikey) {
        this.apikey = apikey;
    }

    public String getSingleSymbolPrice() {
        return singleSymbolPrice;
    }

    public void setSingleSymbolPrice(String singleSymbolPrice) {
        this.singleSymbolPrice = singleSymbolPrice;
    }

    public String getMultipleSymbolFull() {
        return multipleSymbolFull;
    }

    public void setMultipleSymbolFull(String multipleSymbolFull) {
        this.multipleSymbolFull = multipleSymbolFull;
    }

    public String getSingleSymbolHistByType() {
        return singleSymbolHistByType;
    }

    public void setSingleSymbolHistByType(String singleSymbolHistByType) {
        this.singleSymbolHistByType = singleSymbolHistByType;
    }
}
