package com.algotrading.priceengine.cryptocompare.service;

import com.algotrading.priceengine.cryptocompare.CryptoCompareFullSymbol;
import com.algotrading.priceengine.cryptocompare.CryptoComparePrice;

import java.util.List;

public interface CyrptoCompareService {

    CryptoComparePrice getPriceBySymbolCurrency(String symbol, String currency);

    CryptoComparePrice getPriceBySymbolEur(String symbol);

    List<CryptoComparePrice> getOCHLBySymbolCurrency(String symbol, String currency, String type, String interval);

    List<CryptoComparePrice> getOCHLBySymbolEur(String symbol, String type, String interval);

    CryptoCompareFullSymbol getFullCoinInfoByCurrency(String symbol, String currency);

    CryptoCompareFullSymbol getFullCoinInfoEur(String symbol);
}
