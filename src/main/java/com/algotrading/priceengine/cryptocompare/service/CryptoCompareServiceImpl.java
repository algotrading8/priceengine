package com.algotrading.priceengine.cryptocompare.service;

import com.algotrading.priceengine.PrinceEngineUtils;
import com.algotrading.priceengine.cryptocompare.CryptoCompareFullSymbol;
import com.algotrading.priceengine.cryptocompare.CryptoComparePrice;
import com.algotrading.priceengine.cryptocompare.CryptoCompareProp;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.algotrading.priceengine.PriceEngineConstants.EUR;
import static com.algotrading.priceengine.PriceEngineConstants.uriCryptoCompare;

@Service
public class CryptoCompareServiceImpl implements CyrptoCompareService {

    @Autowired
    public CryptoCompareProp cryptoCompareProp;
    @Autowired
    public PrinceEngineUtils princeEngineUtils;

    @Override
    public CryptoComparePrice getPriceBySymbolCurrency(String symbol, String currency){
        String url = uriCryptoCompare + cryptoCompareProp.getSingleSymbolPrice();
        url = url.replace("COIN", symbol);
        url = url.replace("CURRENCIES", currency);
        String result = makeAPICall(url);

        return princeEngineUtils.CryptoComparePriceObject(result, currency, symbol);
    }

    @Override
    public CryptoComparePrice getPriceBySymbolEur(String symbol) {
        return getPriceBySymbolCurrency(symbol, EUR);
    }

    @Override
    public List<CryptoComparePrice> getOCHLBySymbolCurrency(String symbol, String currency, String type, String interval){
        String url = uriCryptoCompare + cryptoCompareProp.getSingleSymbolHistByType();
        url = url.replace("COIN", symbol);
        url = url.replace("CURRENCIES", currency);
        url = url.replace("TYPE", type);
        url = url.replace("INTERVAL", interval);
        String result = makeAPICall(url);

        return princeEngineUtils.CryptoComparePriceOCHL(result, currency, symbol);
    }

    @Override
    public List<CryptoComparePrice> getOCHLBySymbolEur(String symbol, String type, String interval) {
        return getOCHLBySymbolCurrency(symbol, EUR, type, interval);
    }

    @Override
    public CryptoCompareFullSymbol getFullCoinInfoByCurrency(String symbol, String currency) {
        String url = uriCryptoCompare + cryptoCompareProp.getMultipleSymbolFull();
        url = url.replace("COIN", symbol);
        url = url.replace("CURRENCIES", currency);

        String result = makeAPICall(url);
        return princeEngineUtils.CryptoCompareFullSymbol(result, symbol, currency);
    }

    @Override
    public CryptoCompareFullSymbol getFullCoinInfoEur(String symbol) {
        return getFullCoinInfoByCurrency(symbol, EUR);
    }

    private String makeAPICall(String url) {
        String response_content = "";
        URIBuilder query = null;

        try {
            query = new URIBuilder(url);
            CloseableHttpClient client = HttpClients.createDefault();
            HttpGet request = new HttpGet(query.build());

            request.addHeader("Apikey", cryptoCompareProp.getApikey());
            CloseableHttpResponse response = client.execute(request);
            System.out.println(response.getStatusLine());
            HttpEntity entity = response.getEntity();
            response_content = EntityUtils.toString(entity);
            EntityUtils.consume(entity);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return response_content;
    }

}
