package com.algotrading.priceengine.cryptocompare;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CryptoCompareFullSymbol {
    @SerializedName("FROMSYMBOL")
    @Expose
    private String symbol;
    @SerializedName("TOSYMBOL")
    @Expose
    private String currency;
    @SerializedName("MARKET")
    @Expose
    private String market;
    @SerializedName("PRICE")
    @Expose
    private Double price;
    @SerializedName("LASTUPDATE")
    @Expose
    private String lastUpdate;
    @SerializedName("LASTVOLUME")
    @Expose
    private Double lastVolume;
    @SerializedName("MEDIAN")
    @Expose
    private Double median;
    @SerializedName("LASTVOLUMETO")
    @Expose
    private Double lastVolumeTo;
    @SerializedName("LASTTRADEID")
    @Expose
    private Double lastTradeId;
    @SerializedName("VOLUMEDAY")
    @Expose
    private Double volumeDay;
    @SerializedName("VOLUMEDAYTO")
    @Expose
    private Double volumeDayTo;
    @SerializedName("VOLUME24HOUR")
    @Expose
    private Double volumeDay24h;
    @SerializedName("VOLUME24HOURTO")
    @Expose
    private Double volumeDay24hTo;
    @SerializedName("OPENDAY")
    @Expose
    private Double openDay;
    @SerializedName("HIGHDAY")
    @Expose
    private Double highDay;
    @SerializedName("LOWDAY")
    @Expose
    private Double lowDay;
    @SerializedName("OPEN24HOUR")
    @Expose
    private Double open24h;
    @SerializedName("HIGH24HOUR")
    @Expose
    private Double high24h;
    @SerializedName("LOW24HOUR")
    @Expose
    private Double low24h;
    @SerializedName("LASTMARKET")
    @Expose
    private String lastMarket;
    @SerializedName("VOLUMEHOUR")
    @Expose
    private Double volumeHour;
    @SerializedName("VOLUMEHOURTO")
    @Expose
    private Double volumeHourTo;
    @SerializedName("OPENHOUR")
    @Expose
    private Double openHour;
    @SerializedName("HIGHHOUR")
    @Expose
    private Double highHour;
    @SerializedName("LOWHOUR")
    @Expose
    private Double lowHour;
    @SerializedName("TOPTIERVOLUME24HOUR")
    @Expose
    private Double topTierVolume24Hour;
    @SerializedName("TOPTIERVOLUME24HOURTO")
    @Expose
    private Double topTierVolume24HourTo;
    @SerializedName("CHANGE24HOUR")
    @Expose
    private Double change24h;
    @SerializedName("CHANGEPCT24HOUR")
    @Expose
    private Double changePct24h;
    @SerializedName("CHANGEDAY")
    @Expose
    private Double changeDay;
    @SerializedName("CHANGEPCTDAY")
    @Expose
    private Double changePctDay;
    @SerializedName("CHANGEHOUR")
    @Expose
    private Double changeHour;
    @SerializedName("CHANGEPCTHOUR")
    @Expose
    private Double changePctHour;
    @SerializedName("CONVERSIONTYPE")
    @Expose
    private String conversionType;
    @SerializedName("CONVERSIONSYMBOL")
    @Expose
    private String conversionSymbol;
    @SerializedName("SUPPLY")
    @Expose
    private Double supply;
    @SerializedName("MKTCAP")
    @Expose
    private Double mktCap;
    @SerializedName("MKTCAPPENALTY")
    @Expose
    private Double mktCapPenalty;
    @SerializedName("CIRCULATINGSUPPLY")
    @Expose
    private Double circulatingSupply;
    @SerializedName("CIRCULATINGSUPPLYMKTCAP")
    @Expose
    private Double circulatingSupplyMktCap;
    @SerializedName("TOTALVOLUME24H")
    @Expose
    private Double totalVolume24h;
    @SerializedName("TOTALVOLUME24HTO")
    @Expose
    private Double totalVolume24hTo;
    @SerializedName("TOTALTOPTIERVOLUME24H")
    @Expose
    private Double totalTopTierVolume24h;
    @SerializedName("TOTALTOPTIERVOLUME24HTO")
    @Expose
    private Double totalTopTierVolume24hTo;
    @SerializedName("IMAGEURL")
    @Expose
    private String imageUrl;

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getMarket() {
        return market;
    }

    public void setMarket(String market) {
        this.market = market;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Double getLastVolume() {
        return lastVolume;
    }

    public void setLastVolume(Double lastVolume) {
        this.lastVolume = lastVolume;
    }

    public Double getLastVolumeTo() {
        return lastVolumeTo;
    }

    public void setLastVolumeTo(Double lastVolumeTo) {
        this.lastVolumeTo = lastVolumeTo;
    }

    public Double getLastTradeId() {
        return lastTradeId;
    }

    public void setLastTradeId(Double lastTradeId) {
        this.lastTradeId = lastTradeId;
    }

    public Double getVolumeDay() {
        return volumeDay;
    }

    public void setVolumeDay(Double volumeDay) {
        this.volumeDay = volumeDay;
    }

    public Double getVolumeDayTo() {
        return volumeDayTo;
    }

    public void setVolumeDayTo(Double volumeDayTo) {
        this.volumeDayTo = volumeDayTo;
    }

    public Double getVolumeDay24h() {
        return volumeDay24h;
    }

    public void setVolumeDay24h(Double volumeDay24h) {
        this.volumeDay24h = volumeDay24h;
    }

    public Double getVolumeDay24hTo() {
        return volumeDay24hTo;
    }

    public void setVolumeDay24hTo(Double volumeDay24hTo) {
        this.volumeDay24hTo = volumeDay24hTo;
    }

    public Double getOpenDay() {
        return openDay;
    }

    public void setOpenDay(Double openDay) {
        this.openDay = openDay;
    }

    public Double getHighDay() {
        return highDay;
    }

    public void setHighDay(Double highDay) {
        this.highDay = highDay;
    }

    public Double getLowDay() {
        return lowDay;
    }

    public void setLowDay(Double lowDay) {
        this.lowDay = lowDay;
    }

    public Double getOpen24h() {
        return open24h;
    }

    public void setOpen24h(Double open24h) {
        this.open24h = open24h;
    }

    public Double getHigh24h() {
        return high24h;
    }

    public void setHigh24h(Double high24h) {
        this.high24h = high24h;
    }

    public Double getLow24h() {
        return low24h;
    }

    public void setLow24h(Double low24h) {
        this.low24h = low24h;
    }

    public String getLastMarket() {
        return lastMarket;
    }

    public void setLastMarket(String lastMarket) {
        this.lastMarket = lastMarket;
    }

    public Double getVolumeHour() {
        return volumeHour;
    }

    public void setVolumeHour(Double volumeHour) {
        this.volumeHour = volumeHour;
    }

    public Double getVolumeHourTo() {
        return volumeHourTo;
    }

    public void setVolumeHourTo(Double volumeHourTo) {
        this.volumeHourTo = volumeHourTo;
    }

    public Double getOpenHour() {
        return openHour;
    }

    public void setOpenHour(Double openHour) {
        this.openHour = openHour;
    }

    public Double getHighHour() {
        return highHour;
    }

    public void setHighHour(Double highHour) {
        this.highHour = highHour;
    }

    public Double getLowHour() {
        return lowHour;
    }

    public void setLowHour(Double lowHour) {
        this.lowHour = lowHour;
    }

    public Double getTopTierVolume24Hour() {
        return topTierVolume24Hour;
    }

    public void setTopTierVolume24Hour(Double topTierVolume24Hour) {
        this.topTierVolume24Hour = topTierVolume24Hour;
    }

    public Double getTopTierVolume24HourTo() {
        return topTierVolume24HourTo;
    }

    public void setTopTierVolume24HourTo(Double topTierVolume24HourTo) {
        this.topTierVolume24HourTo = topTierVolume24HourTo;
    }

    public Double getChange24h() {
        return change24h;
    }

    public void setChange24h(Double change24h) {
        this.change24h = change24h;
    }

    public Double getChangePct24h() {
        return changePct24h;
    }

    public void setChangePct24h(Double changePct24h) {
        this.changePct24h = changePct24h;
    }

    public Double getChangeDay() {
        return changeDay;
    }

    public void setChangeDay(Double changeDay) {
        this.changeDay = changeDay;
    }

    public Double getChangePctDay() {
        return changePctDay;
    }

    public void setChangePctDay(Double changePctDay) {
        this.changePctDay = changePctDay;
    }

    public Double getChangeHour() {
        return changeHour;
    }

    public void setChangeHour(Double changeHour) {
        this.changeHour = changeHour;
    }

    public Double getChangePctHour() {
        return changePctHour;
    }

    public void setChangePctHour(Double changePctHour) {
        this.changePctHour = changePctHour;
    }

    public String getConversionType() {
        return conversionType;
    }

    public void setConversionType(String conversionType) {
        this.conversionType = conversionType;
    }

    public String getConversionSymbol() {
        return conversionSymbol;
    }

    public void setConversionSymbol(String conversionSymbol) {
        this.conversionSymbol = conversionSymbol;
    }

    public Double getSupply() {
        return supply;
    }

    public void setSupply(Double supply) {
        this.supply = supply;
    }

    public Double getMktCap() {
        return mktCap;
    }

    public void setMktCap(Double mktCap) {
        this.mktCap = mktCap;
    }

    public Double getMktCapPenalty() {
        return mktCapPenalty;
    }

    public void setMktCapPenalty(Double mktCapPenalty) {
        this.mktCapPenalty = mktCapPenalty;
    }

    public Double getCirculatingSupply() {
        return circulatingSupply;
    }

    public void setCirculatingSupply(Double circulatingSupply) {
        this.circulatingSupply = circulatingSupply;
    }

    public Double getCirculatingSupplyMktCap() {
        return circulatingSupplyMktCap;
    }

    public void setCirculatingSupplyMktCap(Double circulatingSupplyMktCap) {
        this.circulatingSupplyMktCap = circulatingSupplyMktCap;
    }

    public Double getTotalVolume24h() {
        return totalVolume24h;
    }

    public void setTotalVolume24h(Double totalVolume24h) {
        this.totalVolume24h = totalVolume24h;
    }

    public Double getTotalVolume24hTo() {
        return totalVolume24hTo;
    }

    public void setTotalVolume24hTo(Double totalVolume24hTo) {
        this.totalVolume24hTo = totalVolume24hTo;
    }

    public Double getTotalTopTierVolume24h() {
        return totalTopTierVolume24h;
    }

    public void setTotalTopTierVolume24h(Double totalTopTierVolume24h) {
        this.totalTopTierVolume24h = totalTopTierVolume24h;
    }

    public Double getTotalTopTierVolume24hTo() {
        return totalTopTierVolume24hTo;
    }

    public void setTotalTopTierVolume24hTo(Double totalTopTierVolume24hTo) {
        this.totalTopTierVolume24hTo = totalTopTierVolume24hTo;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Double getMedian() {
        return median;
    }

    public void setMedian(Double median) {
        this.median = median;
    }
}
