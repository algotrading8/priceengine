package com.algotrading.priceengine;

import org.springframework.stereotype.Component;

@Component
public class PriceEngineConstants {
    public static final String EUR = "EUR";
    public static final String CRYPTOCOMPARE = "CRYPTOCOMPARE";
    public static final String MINUTE = "minute";
    public static final String HOUR = "hour";
    public static final String DAY = "day";
    public static final String uriCryptoCompare = "https://min-api.cryptocompare.com/data";
    public static final Double coinbaseFee = 1.6;
}
