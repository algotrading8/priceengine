package com.algotrading.priceengine.statistics.controller;

import com.algotrading.priceengine.statistics.PriceScoring;
import com.algotrading.priceengine.statistics.PriceScoringProcess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping({"/statistics"})
public class StatisticsController {

    @Autowired
    public PriceScoringProcess priceScoringProcess;

    @RequestMapping(value="/initStatistics/{type}/{symbol}/{interval}", method = RequestMethod.GET)
    public PriceScoring initStatistics(@PathVariable(value="type") String type,
                                       @PathVariable(value="symbol") String symbol,
                                       @PathVariable(value="interval") String interval) {
        priceScoringProcess.setInterval(interval);
        priceScoringProcess.setSymbol(symbol);
        priceScoringProcess.setSymbol(symbol);
        return priceScoringProcess.initStatistics(type);
    }
}
