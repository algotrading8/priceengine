package com.algotrading.priceengine.statistics;

import com.algotrading.priceengine.cryptocompare.CryptoComparePrice;
import com.algotrading.priceengine.cryptocompare.service.CyrptoCompareService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.OptionalDouble;
import java.util.stream.Collectors;

@Component
public class PriceScoringProcess {

    public PriceScoring priceScoring = new PriceScoring();
    public String symbol;
    public String interval;

    @Autowired
    public CyrptoCompareService cyrptoCompareService;

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getInterval() {
        return interval;
    }

    public void setInterval(String interval) {
        this.interval = interval;
    }

    public PriceScoring initStatistics(String type) {
        List<CryptoComparePrice> cryptoComparePriceList = cyrptoCompareService.getOCHLBySymbolEur(symbol, type, interval);
        fillPriceScoring(cryptoComparePriceList);

        return priceScoring;
    }

    private void fillPriceScoring(List<CryptoComparePrice> cryptoComparePriceList) {
        int size = cryptoComparePriceList.size();
        priceScoring.setInterval(size);
        List<Double> meanList = cryptoComparePriceList.stream()
                .map(CryptoComparePrice -> (CryptoComparePrice.getLow() + CryptoComparePrice.getHigh())/2)
                .collect(Collectors.toList());
        OptionalDouble mean = meanList.stream()
                .mapToDouble(a -> a)
                .average();
        Double percentChange = cryptoComparePriceList.get(size - 1).getPrice() / cryptoComparePriceList.get(0).getPrice();

        priceScoring.setMean(mean.isPresent() ? mean.getAsDouble() : 0);
        Double variance = cryptoComparePriceList.stream()
                        .mapToDouble(CryptoComparePrice -> Math.pow((CryptoComparePrice.getLow() + CryptoComparePrice.getHigh())/2 - priceScoring.getMean(), 2))
                        .sum();

        priceScoring.setPercentChange(percentChange);
        priceScoring.setVariance(Math.pow(variance/size, 0.5));
        priceScoring.setTrust(1.);
    }
}
