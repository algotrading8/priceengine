package com.algotrading.priceengine;

import com.algotrading.priceengine.cryptocompare.CryptoCompareFullSymbol;
import com.algotrading.priceengine.cryptocompare.CryptoComparePrice;
import com.google.gson.Gson;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.algotrading.priceengine.PriceEngineConstants.CRYPTOCOMPARE;

@Component
public class PrinceEngineUtils {

    public CryptoComparePrice CryptoComparePriceObject(String result, String currency, String symbol) {
        CryptoComparePrice priceObject = new CryptoComparePrice();
        try {
            JSONObject json = new JSONObject(result);
            Double price = json.getDouble(currency);
            priceObject.setPrice(price);
            priceObject.setCurrency(currency);
            priceObject.setSymbol(symbol);
            priceObject.setTimestamp(new Date());
            priceObject.setValid(true);
            priceObject.setSource(CRYPTOCOMPARE);
        } catch (Exception e){
            e.printStackTrace();
        }

        return priceObject;
    }

    public CryptoCompareFullSymbol CryptoCompareFullSymbol(String result, String symbol, String currency) {
        CryptoCompareFullSymbol fullSymbol = new CryptoCompareFullSymbol();
        Gson gson = new Gson();

        try{
            JSONObject jsonResult = new JSONObject(result);
            JSONObject jsonObject = jsonResult.getJSONObject("RAW").getJSONObject(symbol).getJSONObject(currency);
            fullSymbol = gson.fromJson(jsonObject.toString(), CryptoCompareFullSymbol.class);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return fullSymbol;
    }

    public List<CryptoComparePrice> CryptoComparePriceOCHL(String result, String currency, String symbol) {
        List<CryptoComparePrice> priceObjectList = new ArrayList<>();
        Gson gson = new Gson();

        try {
            JSONObject jsonObject = new JSONObject(result);
            JSONArray jsonArray = jsonObject.getJSONObject("Data").getJSONArray("Data");

            for (int i = 0, size = jsonArray.length(); i < size; i++) {
                JSONObject json = jsonArray.getJSONObject(i);
                CryptoComparePrice priceObject = gson.fromJson(json.toString(), CryptoComparePrice.class);
                priceObject.setCurrency(currency);
                priceObject.setSymbol(symbol);
                priceObject.setPrice(0.5 * priceObject.getHigh() + 0.5 * priceObject.getLow());
                Long timeStamp = json.getLong("time");
                priceObject.setTimestamp(new Date(timeStamp*1000));
                priceObject.setValid(true);
                priceObject.setSource(CRYPTOCOMPARE);
                priceObjectList.add(priceObject);
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        return priceObjectList;
    }
}
